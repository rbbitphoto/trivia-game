import os
import psycopg 

CONNECTION_STR = f"dbname={os.getenv('PGDATABASE')} user={os.getenv('PGUSER')} password={os.getenv('PGUSER')}"

def query(f):
    def wrapper(*args, **kwargs):
        with psycopg.connect(CONNECTION_STR) as conn:
            with conn.cursor() as cur:
                return cur.execute(*f(*args, **kwargs)).fetchall()
    return wrapper