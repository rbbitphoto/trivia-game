from fastapi import FastAPI
from pydantic import BaseModel
import psycopg 

# class Categories(BaseModel):
#     id: int
#     title: str(250)
#     canon: bool

app = FastAPI()

@app.get("/api/categories")
def get_categories_first_100(page: int = 0):
    with psycopg.connect("dbname=trivia-game user=trivia-game") as conn:
        with conn.cursor() as cur:
            return cur.execute("""
                select row_to_json(categories) FROM categories
                LIMIT 100
                OFFSET %s
            """, (page * 100,)).fetchall()


# @app.post("/api/categories")
# def get_categories_first_100(name: str):
#     with psycopg.connect("dbname=trivia-game user=trivia-game") as conn:
#         with conn.cursor() as cur:
#             cur.execute("""
#                 select * FROM categories
#                 LIMIT 100
#                 OFFSET %s
#             """, (page * 100,))
#             return cur.fetchall()

